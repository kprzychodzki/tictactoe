# TicTacToe - Info #

## Basic information ##
This a Maven project, written in Java 8 with GUI created in JavaFX. Simple MVC type application with possibility for further develop.

### Description ###

TicTacToe - simple JavaFx application for standard 3x3 fields TicTacToe game. However implementation of the method checking the result of actual board situation allows to create different board dimensions like 4x4 or 5x5 and even bigger. This is a scope for further consideration. 

Below example of mentioned above method, check if all fields in the row are marked by the same player.

```
#!java

 public boolean checkResult(Player player) {

        int n = sizeOfBoard;
        double k = Math.sqrt(sizeOfBoard);
       
        for (int i = 0; i < n; i += k) {
            int sumOfRows = 1;
            for (int j = i; j < k + i; j++) {
                sumOfRows *= fieldList.get(j);
            }
            if (sumOfRows == player.getValue()) {
                return true;
            }

        }
```