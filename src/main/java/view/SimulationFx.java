package view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Game;

import java.io.IOException;

/**
 *
 */

public class SimulationFx extends Application{

    static Game game;
    public static void main(String[] args) {
        game = new Game();
        Application.launch();
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = fxmlSceneGraph();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();

    }

    private Parent fxmlSceneGraph() throws IOException{
        return FXMLLoader.load(getClass().getResource("/tictactoe.fxml"));

    }
}
