package view;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import java.util.List;

import static view.SimulationFx.game;

/**
 *
 */
public class tictactoeController {

    @FXML
    private Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8;

    @FXML
    private Label labelWinner;

    @FXML
    private Label labelPlayer;

    @FXML
    private Label labelRound;


    public void setLabelPlayer() {
        labelPlayer.setText(game.getCurrentPlayer().getName());
    }

    @FXML
    private void initialize() {
        setLabelPlayer();
        labelRound.setText("1");
        labelWinner.setText("");
    }


    @FXML
    void buttonPressed(ActionEvent event) {

        Button button = (Button) event.getTarget();

        String buttonIdText = button.getId();
        int buttonId = Integer.parseInt(buttonIdText.substring(3));


        List<Integer> completeList = game.userChoice(buttonId);
        setLabelPlayer();
        labelRound.setText(String.valueOf(game.getRound()));

        if (completeList.get(buttonId) == 2) {
            button.setText("X");
        } else if (completeList.get(buttonId) == 3) {
            button.setText("O");
        }


        if (game.getWinner() != null) {
            labelWinner.setText(game.getWinner().getName());
            uncheckAble();


        } else if (game.getRound() == 10) {
            labelWinner.setText("Draw");
            uncheckAble();
        }

    }

    private void uncheckAble() {
        btn0.setDisable(true);
        btn1.setDisable(true);
        btn2.setDisable(true);
        btn3.setDisable(true);
        btn4.setDisable(true);
        btn5.setDisable(true);
        btn6.setDisable(true);
        btn7.setDisable(true);
        btn8.setDisable(true);
    }

    private void checkAble() {
        btn0.setDisable(false);
        btn1.setDisable(false);
        btn2.setDisable(false);
        btn3.setDisable(false);
        btn4.setDisable(false);
        btn5.setDisable(false);
        btn6.setDisable(false);
        btn7.setDisable(false);
        btn8.setDisable(false);
    }

    @FXML
    void newGamePressed(ActionEvent event) {
        game.newGame();
        game.setWinner(null);
        labelRound.setText("1");
        labelWinner.setText("");
        checkAble();
        btn0.setText("");
        btn1.setText("");
        btn2.setText("");
        btn3.setText("");
        btn4.setText("");
        btn5.setText("");
        btn6.setText("");
        btn7.setText("");
        btn8.setText("");


    }

    @FXML
    void buttonExitPressed(ActionEvent event) {
        Platform.exit();
    }


}
