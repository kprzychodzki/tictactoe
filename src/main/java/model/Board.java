package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */

public class Board {

    private List<Integer> fieldList;
    private int sizeOfBoard;


    public Board(int sizeOfBoard) {
        this.sizeOfBoard = sizeOfBoard;

        fieldList = new ArrayList<>(sizeOfBoard);

        for (int i = 0; i < sizeOfBoard; i++) {
            fieldList.add(i, 1);
        }

    }

    public List<Integer> getFieldList() {
        return fieldList;
    }

    protected List<Integer> cleanBoard() {
        fieldList.clear();
        for (int i = 0; i < sizeOfBoard; i++) {
            fieldList.add(i, 1);
        }
        return fieldList;
    }

    protected List<Integer> arrayOfPlayersSymbols(int specifiedField, Player playerSymbol) {

        if (fieldList.get(specifiedField) == 1) {
            if (playerSymbol.getSymbol() == Symbol.X) {
                fieldList.set(specifiedField, 2);
            } else {
                fieldList.set(specifiedField, 3);
            }
        } else {
            // what if field is occupied?
        }

        return fieldList;

    }

    protected boolean insertPlayerSymbol(int specifiedField, Player playerSymbol) {

        if (fieldList.get(specifiedField) == 1) {
            if (playerSymbol.getSymbol() == Symbol.X) {
                fieldList.set(specifiedField, 2);
            } else {
                fieldList.set(specifiedField, 3);
            }
            return true;
        } else {
            // what if field is occupied?
            return false;
        }

    }


    public boolean checkResult(Player player) {

        int n = sizeOfBoard;
        double k = Math.sqrt(sizeOfBoard);
        int sum = 1;
        int sumOfSecondDiagonal = 1;


        for (int i = 0; i < n; i += k) {
            int sumOfRows = 1;
            for (int j = i; j < k + i; j++) {
                sumOfRows *= fieldList.get(j);
            }
            if (sumOfRows == player.getValue()) {
                System.out.println("row");
                return true;
            }

        }


        for (int i = 0; i < k; i++) {
            int sumOfCol = 1;
            for (int j = i; j < n; j += k) {
                sumOfCol *= fieldList.get(j);
            }
            if (sumOfCol == player.getValue()) {
                System.out.println("col");
                return true;
            }

        }

        for (int i = 0; i < n; i += k + 1) {
            int sumOfFirstDiagonal = fieldList.get(i);
            sum *= sumOfFirstDiagonal;
            if (sum == player.getValue()) {
                System.out.println("1diag");
                return true;
            }
        }


        for (int i = (int) (k - 1); i < n; i += k - 1) {
            sumOfSecondDiagonal *= fieldList.get(i);
            if (sumOfSecondDiagonal == player.getValue()) {
                System.out.println("2diag");
                return true;
            }
        }


        return false;


    }


}
