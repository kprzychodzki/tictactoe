package model;

/**
 *
 */
public class Player {
    private String name;
    private Symbol symbol;
    private double value;

    public Player(String name, Symbol symbol, double value) {
        this.name = name;
        this.symbol = symbol;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public double getValue() {
        return value;
    }
}
