package model;


import java.util.List;

/**
 *
 */
public class Game {

    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Board board;
    private int round;
    private int sizeOfBoard;
    private Player winner;

    public Game() {

        sizeOfBoard = 9;
        double s = Math.sqrt(sizeOfBoard);
        double playerOValue = Math.pow(3, s);
        double playerXValue = Math.pow(2, s);

        board = new Board(sizeOfBoard);

        playerO = new Player("Ring", Symbol.O, playerOValue);
        playerX = new Player("Cross", Symbol.X, playerXValue);

        currentPlayer = playerO;
        round = 1;

    }

    public List<Integer> userChoice(int specifiedField) {
        List<Integer> listOfFields;

        if (board.insertPlayerSymbol(specifiedField, currentPlayer)) {
            System.out.println(board.checkResult(currentPlayer));
            System.out.println(board.getFieldList());
            if (board.checkResult(currentPlayer)) {
                System.out.println("Winner: " + currentPlayer.getName());
                setWinner(currentPlayer);
            }
            if (currentPlayer == playerO) {
                currentPlayer = playerX;
            } else {
                currentPlayer = playerO;
            }
            round++;
        }

        listOfFields = board.arrayOfPlayersSymbols(specifiedField, currentPlayer);


        return listOfFields;
    }

    public void newGame() {
        board.cleanBoard();
        round = 1;

    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public int getRound() {
        return round;
    }

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }
}
